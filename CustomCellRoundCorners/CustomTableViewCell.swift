import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var btnStatus: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.btnStatus.roundCorners(corners: [.bottomRight], radius: 7.0, borderWidth: nil, borderColor: nil)
        
        // Initialization code
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
