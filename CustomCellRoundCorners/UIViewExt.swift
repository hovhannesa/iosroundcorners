import Foundation
import UIKit

extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat, borderWidth: CGFloat?, borderColor: UIColor?) {
        let maskPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let maskLayer = CAShapeLayer()
        maskLayer.frame = self.bounds
        maskLayer.path = maskPath.cgPath
        self.layer.mask = maskLayer
        self.layer.masksToBounds = true
        
        if (borderWidth != nil && borderColor != nil) {
            let borderLayer = CAShapeLayer()
            borderLayer.frame = self.bounds;
            borderLayer.path  = maskPath.cgPath;
            borderLayer.lineWidth = borderWidth!;
            borderLayer.strokeColor = borderColor!.cgColor;
            borderLayer.fillColor   = UIColor.clear.cgColor;
            
            self.layer.addSublayer(borderLayer);
        }
        
    }
}

